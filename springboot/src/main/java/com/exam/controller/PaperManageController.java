package com.exam.controller;

import com.exam.entity.ApiResult;
import com.exam.entity.PaperManage;
import com.exam.service.PaperManageService;
import com.exam.util.ApiResultHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author;XCL
 * @Date; 2022/11/25 - 17:03
 * @Description: com.exam.controller
 */
@RestController
public class PaperManageController {
    @Autowired
    private PaperManageService paperManageService;

    @PostMapping("/paperManage")
    public ApiResult add(@RequestBody PaperManage paperManage){
        int paperResult = paperManageService.add(paperManage);
        if(paperResult != 0){
            return ApiResultHandler.buildApiResult(200,"添加成功",paperResult);
        }
        System.out.println("添加失败");
        return ApiResultHandler.buildApiResult(400,"添加失败",paperResult);
    }
}
