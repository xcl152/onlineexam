package com.exam.controller;

import com.exam.entity.ApiResult;
import com.exam.entity.PaperManage;
import com.exam.service.FillQuestionService;
import com.exam.service.JudgeQuestionService;
import com.exam.service.MultiQuestionService;
import com.exam.service.PaperManageService;
import com.exam.util.ApiResultHandler;
import com.exam.vo.ItemVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author;XCL
 * @Date; 2022/11/30 - 9:23
 * @Description: 组合一张试卷
 */
@RestController
public class ItemController {
    //注入其他类的service层，方便调用里面的方法
    @Autowired
    private MultiQuestionService multiQuestionService;
    @Autowired
    private FillQuestionService fillQuestionService;
    @Autowired
    private JudgeQuestionService judgeQuestionService;
    @Autowired
    private PaperManageService paperManageService;

    @PostMapping("/item")
    public ApiResult addItem(@RequestBody ItemVo itemVo){
        //获取前端传过来的选择题数量
        Integer changeNumber = itemVo.getChangeNumber();
        //获取前端传过来的填空题数量
        Integer fillNumber = itemVo.getFillNumber();
        //获取前端传过来的判断题数量
        Integer judgeNumber = itemVo.getJudgeNumber();
        //获取前端传过来的试卷id
        Integer paperId = itemVo.getPaperId();

        //从选择题库拿出题目添加进paper_manage表中
        List<Integer> multiQuestionIds = multiQuestionService.getBySubject(itemVo.getSubject(),changeNumber);
        if(multiQuestionIds == null){
            return ApiResultHandler.buildApiResult(400,"选择题数据库获取失败",null);
        }
        for(Integer multiQuestionId:multiQuestionIds){
            PaperManage paperManage = new PaperManage(paperId,1,multiQuestionId);
            int addMultiResult = paperManageService.add(paperManage);
            if(addMultiResult == 0){
                return ApiResultHandler.buildApiResult(400,"选择题组卷保存失败",null);
            }
        }
        //从填空题题库拿出题目添加进paper_manage表中
        List<Integer> fillQuestionIds = fillQuestionService.getBySubject(itemVo.getSubject(),fillNumber);
        if(fillQuestionIds == null){
            return ApiResultHandler.buildApiResult(400,"填空题数据库获取失败",null);
        }
        for(Integer fillQuestionId:fillQuestionIds){
            PaperManage paperManage = new PaperManage(paperId,2,fillQuestionId);
            int addFillResult = paperManageService.add(paperManage);
            if(addFillResult == 0){
                return ApiResultHandler.buildApiResult(400,"填空题组卷保存失败",null);
            }
        }
        //从判断题题库拿出题目添加进paper_manage表中
        List<Integer> judgeQuestionIds = judgeQuestionService.getBySubject(itemVo.getSubject(),judgeNumber);
        if(judgeQuestionIds == null){
            return ApiResultHandler.buildApiResult(400,"判断题数据库获取失败",null);
        }
        for(Integer judgeQuestionId:judgeQuestionIds){
            PaperManage paperManage = new PaperManage(paperId,2,judgeQuestionId);
            int addJudgeResult = paperManageService.add(paperManage);
            if(addJudgeResult == 0){
                return ApiResultHandler.buildApiResult(400,"判断题组卷保存失败",null);
            }
        }
        return ApiResultHandler.buildApiResult(200,"试卷组卷成功",null);
    }
}
