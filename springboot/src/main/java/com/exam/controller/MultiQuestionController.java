package com.exam.controller;

import com.exam.entity.ApiResult;
import com.exam.entity.MultiQuestion;
import com.exam.service.MultiQuestionService;
import com.exam.util.ApiResultHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author;XCL
 * @Date; 2022/11/25 - 15:42
 * @Description: com.exam.controller
 */
@RestController
public class MultiQuestionController {

    @Autowired
    private MultiQuestionService multiQuestionService;

    @PostMapping("/MultiQuestion")
    public ApiResult addMultiQuestion(@RequestBody MultiQuestion multiQuestion){
        int multiId = multiQuestionService.add(multiQuestion);
        //判断multiId是否有数据
        if(multiId != 0){
            return ApiResultHandler.buildApiResult(200,"添加成功",multiId);
        }
        return ApiResultHandler.buildApiResult(400,"添加失败",multiId);
    }

    /*
    * 查询最后一条记录的questionId，
    * 刚添加进去的选择题id
    * */
    @GetMapping("/multiQuestionId")
    public ApiResult findMultiQuestionId(){
        MultiQuestion multId = multiQuestionService.find();
        System.out.println("questionId为："+multId);
        return ApiResultHandler.buildApiResult(200,"查询成功",multId);
    }

}
