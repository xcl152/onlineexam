package com.exam.entity;

import lombok.Data;

/**
 * @author;XCL
 * @Date; 2022/11/25 - 15:43
 * @Description: com.exam.entity
 */
@Data
public class MultiQuestion {
    /*试题编号*/
    private Integer questionId;
    /*考试科目*/
    private String subject;
    /*所属章节*/
    private String section;
    /*选项A*/
    private String answerA;

    private String answerB;

    private String answerC;

    private String answerD;
    /*问题题目*/
    private String question;
    /*难度等级*/
    private String level;
    /*正确答案*/
    private String rightAnswer;

    private String analysis; //题目解析
    /*分数*/
    private Integer score;
}
