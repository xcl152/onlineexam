package com.exam.entity;

import lombok.Data;

/**
 * @author;XCL
 * @Date; 2022/11/18 - 16:35
 * @Description: com.exam.entity
 */
@Data
public class Question {
    /*
    * 试题编号
    * */
    private Integer questionId;

    /* 考试科目 */
    private String subject;

    /*问题题目*/
    private String question;

    /*正确答案*/
    private String rightAnswer;

    /*题目解析*/
    private String analysis;

    /*分数*/
    private Integer score;

    /*所属章节*/
    private String section;

    /*难度等级*/
    private String level;


}
