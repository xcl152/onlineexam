package com.exam.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author;XCL
 * @Date; 2022/11/25 - 17:00
 * @Description: com.exam.entity
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PaperManage {
    /*试卷编号*/
    private Integer paperId;
    /*题目类型*/
    private Integer questionType;
    /*题目编号*/
    private Integer questionId;

}
