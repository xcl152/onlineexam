package com.exam.entity;

import lombok.Data;

/**
 * @author;XCL
 * @Date; 2022/11/24 - 10:38
 * @Description: com.exam.entity
 */
@Data
public class ExamManage {
    /*考试编号*/
    private Integer examCode;
    /*该次考试介绍*/
    private String description;
    /*课程名称*/
    private String source;
    /*考试编号*/
    private String paperId;
    /*考试日期*/
    private String examDate;
    /*持续时长*/
    private String totalTime;
    /*年级*/
    private String grade;
    /*学期*/
    private String term;
    /*专业*/
    private String major;
    /*学院*/
    private String institute;
    /*总分*/
    private String totalScore;
    /*考试类型*/
    private String type;
    /*考试须知*/
    private String tips;

}
