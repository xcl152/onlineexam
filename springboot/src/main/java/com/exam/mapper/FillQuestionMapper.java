package com.exam.mapper;

import com.exam.entity.FillQuestion;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author;XCL
 * @Date; 2022/11/28 - 10:14
 * @Description: com.exam.mapper
 */
@Mapper
public interface FillQuestionMapper {
    //添加填空题库
    @Options(useGeneratedKeys = true,keyProperty ="questionId" )
    @Insert("insert into fill_question(subject,question,answer,analysis,level,section)"+
            "values (#{subject},#{question},#{answer},#{analysis},#{level},#{section})")
    int add(FillQuestion fillQuestion);

    /*
     *  查询最后一条记录的questionId
     *   获取添加进去的填空题的id
     * */
    @Select("select questionId FROM fill_question ORDER BY questionId DESC LIMIT 1")
    FillQuestion get();

    //从填空题库中获取填空题id
    @Select("select questionId from fill_question where subject=#{subject} order by rand() desc limit #{fillNumber}")
    List<Integer> getBySubject(String subject, Integer fillNumber);
}
