package com.exam.mapper;

import com.exam.entity.PaperManage;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author;XCL
 * @Date; 2022/11/25 - 17:03
 * @Description: com.exam.mapper
 */
@Mapper
public interface PaperManageMapper {
    //添加试卷的一个选择题
    @Insert("insert into paper_manage(paperId,questionType,questionId)"+
            "values(#{paperId},#{questionType},#{questionId})")
    int add(PaperManage paperManage);
}
