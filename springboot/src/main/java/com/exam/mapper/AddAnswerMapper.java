package com.exam.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.entity.ExamManage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @author;XCL
 * @Date; 2022/11/24 - 10:47
 * @Description: com.exam.mapper
 */
@Mapper
public interface AddAnswerMapper {

    //分页查询所有考试信息
    @Select("select * from exam_manage")
    IPage<ExamManage> add(Page<Object> pageParam);
}
