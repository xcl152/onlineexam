package com.exam.vo;

import lombok.Data;

/**
 * @author;XCL
 * @Date; 2022/11/30 - 9:20
 * @Description: 题目模型
 */
@Data
public class ItemVo {
    //科目名称
    private String subject;
    /*试卷id*/
    private Integer paperId;
    /*选择题数量*/
    private Integer changeNumber;
    /*填空题数量*/
    private Integer fillNumber;
    /*判断题数量*/
    private Integer judgeNumber;
}
