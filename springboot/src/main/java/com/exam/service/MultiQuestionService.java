package com.exam.service;

import com.exam.entity.MultiQuestion;

import java.util.List;

/**
 * @author;XCL
 * @Date; 2022/11/25 - 15:43
 * @Description: com.exam.service
 */
public interface MultiQuestionService {
    //添加选择题
    int add(MultiQuestion multiQuestion);

    //获取选择题的题目id
    MultiQuestion find();

    //获取选择题库里面的题目id
    List<Integer> getBySubject(String subject, Integer changeNumber);
}
