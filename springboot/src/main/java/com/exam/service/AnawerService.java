package com.exam.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.vo.AnswerVo;

/**
 * @author;XCL
 * @Date; 2022/11/21 - 15:19
 * @Description: com.exam.service
 */
public interface AnawerService {

    //查询所有题库
    IPage<AnswerVo> getAllAnswer(Page<AnswerVo> answerVoPage);
}
