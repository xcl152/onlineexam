package com.exam.service;

import com.exam.entity.FillQuestion;

import java.util.List;

/**
 * @author;XCL
 * @Date; 2022/11/28 - 10:14
 * @Description: com.exam.service
 */
public interface FillQuestionService {
    //添加填空题库
    int add(FillQuestion fillQuestion);

    //获取添加进去的填空题的id
    FillQuestion get();

    //从填空题库中获取填空题id
    List<Integer> getBySubject(String subject, Integer fillNumber);
}
