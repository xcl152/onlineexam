package com.exam.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.entity.ExamManage;

/**
 * @author;XCL
 * @Date; 2022/11/24 - 10:47
 * @Description: com.exam.service
 */
public interface AddAnswerService {
    //分页查询所有考试信息
    IPage<ExamManage> add(Page<Object> pageParam);
}
