package com.exam.serviceimpl;

import com.exam.entity.JudgeQuestion;
import com.exam.mapper.JudgeQuestionMapper;
import com.exam.service.JudgeQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author;XCL
 * @Date; 2022/11/29 - 15:15
 * @Description: com.exam.serviceimpl
 */
@Service
public class JudgeQuestionServiceImpl implements JudgeQuestionService {
    @Autowired
    private JudgeQuestionMapper judgeQuestionMapper;

    //添加一个判断题到题库
    @Override
    public int add(JudgeQuestion judgeQuestion) {
        return judgeQuestionMapper.add(judgeQuestion);
    }

    //获取添加进去的判断题的id
    @Override
    public JudgeQuestion get() {
        return judgeQuestionMapper.get();
    }

    //从判断题库中获取判断题的id
    @Override
    public List<Integer> getBySubject(String subject, Integer judgeNumber) {
        return judgeQuestionMapper.getBySubject(subject,judgeNumber);
    }
}
