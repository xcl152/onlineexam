package com.exam.serviceimpl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.entity.ExamManage;
import com.exam.mapper.AddAnswerMapper;
import com.exam.service.AddAnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author;XCL
 * @Date; 2022/11/24 - 10:47
 * @Description: com.exam.serviceimpl
 */
@Service
public class AddAnswerServiceImpl implements AddAnswerService {

    @Autowired
    private AddAnswerMapper addAnswerMapper;

    //分页查询所有考试信息
    @Override
    public IPage<ExamManage> add(Page<Object> pageParam) {
        return addAnswerMapper.add(pageParam);
    }
}
